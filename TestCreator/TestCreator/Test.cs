﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCreator
{
    class Test
    {
        private string name;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        private bool answerOption;

        private bool showCorrectAnswer;

        private bool areTimerNeeded;

        private DateTime timer;

        //private List<Topic> topicList;

        private int countOfTopics = 0;
        public Test(string name, bool answerOption, bool showCorrectAnswer, bool areTimerNeeded/*, DateTime timer*/)
        {
            this.name = name;
            this.answerOption = answerOption;
            this.showCorrectAnswer = showCorrectAnswer;
            this.areTimerNeeded = areTimerNeeded;
            //this.timer = timer;
        }
    }
}
