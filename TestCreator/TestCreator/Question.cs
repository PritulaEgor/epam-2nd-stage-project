﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCreator
{
    class Question
    {

        public Question (string text, int countOfAnswers, int countOfRightAnswers, string testName, string topicName, string subTopicName, int number)
        {
            this.text = text;

            this.countOfAnswers = countOfAnswers;

            this.countOfRightAnswers = countOfRightAnswers;

            this.testName = testName;

            this.topicName = topicName;

            this.subTopicName = subTopicName;

            this.number = number;
        }
        private int number = 0;

        public int Number
        {
            get
            {
                return number;
            }
            set
            {
                if (value > 0)
                {
                    number = value;
                }
            }
        }

        private string text;

        private int countOfAnswers;

        private int countOfRightAnswers;

        private string testName;

        public string TestName
        {
            get
            {
                return testName;
            }
            set
            {
                testName = value;
            }
        }

        private string topicName;
        public string TopicName
        {
            get
            {
                return topicName;
            }
            set
            {
                topicName = value;
            }
        }

        private string subTopicName="";
        public string SubTopicName
        {
            get
            {
                return subTopicName;
            }
            set
            {
                subTopicName = value;
            }
        }

        //private List<Answer> answersList;
    }
}
