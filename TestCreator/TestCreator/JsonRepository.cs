﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TestCreator
{
    class JsonRepository
    {
        private string filePath;

        public JsonRepository(Test test)
        {
            if (!Directory.Exists(@$"../Tests/{test.Name}"))
            {
                Directory.CreateDirectory(@$"../Tests/{test.Name}");
            }
            this.filePath = @$"../Tests/{test.Name}/{test.Name}.json";
        }

        public JsonRepository(Topic topic)
        {
            if (!Directory.Exists(@$"../Tests/{topic.testName}/{topic.Name}"))
            {
                Directory.CreateDirectory(@$"../Tests/{topic.testName}/{topic.Name}");
            }
            this.filePath = @$"../Tests/{topic.testName}/{topic.Name}/{topic.Name}.json";
        }

        public JsonRepository(SubTopic subTopic)
        {
            if (!Directory.Exists(@$"../Tests/{subTopic.TestName}/{subTopic.topicName}/{subTopic.Name}"))
            {
                Directory.CreateDirectory(@$"../Tests/{subTopic.TestName}/{subTopic.topicName}/{subTopic.Name}");
            }
            this.filePath = @$"../Tests/{subTopic.TestName}/{subTopic.topicName}/{subTopic.Name}/{subTopic.Name}.json";
        }

        public JsonRepository(Question question)
        {
            if (question.SubTopicName != "")
            {
                if (!Directory.Exists(@$"../Tests/{question.TestName}/{question.TopicName}/{question.SubTopicName}/{question.Number} question"))
                {
                    Directory.CreateDirectory(@$"../Tests/{question.TestName}/{question.TopicName}/{question.SubTopicName}/{question.Number} question");
                }

                this.filePath = @$"../Tests/{question.TestName}/{question.TopicName}/{question.SubTopicName}/{question.Number} question/{question.Number}.json";
            }
        }
        public void CreateFile()
        {
                if (!File.Exists(filePath))
                {
                    File.Create(filePath);
                }
           
        }
    }
}
