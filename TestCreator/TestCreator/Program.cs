﻿using System;
using System.IO;

namespace TestCreator
{
    class Program
    {
        static void Main(string[] args)
        {

            Test test = new Test("Test test", true, true, true);
            Topic topic = new Topic(test.Name, "1st topic");
            SubTopic stopic = new SubTopic(topic.Name, test.Name, "1st subtopic of 1st topic");
            Question question = new Question("test", 4, 1, test.Name, topic.Name, stopic.Name, 1);
            Question question1 = new Question("test", 4, 1, test.Name, topic.Name, stopic.Name, 2);
            JsonRepository js = new JsonRepository(test);
            JsonRepository js1 = new JsonRepository(topic);
            JsonRepository js2 = new JsonRepository(stopic);
            JsonRepository js3 = new JsonRepository(question);
         
            js.CreateFile();
            js1.CreateFile();
            js2.CreateFile();
            js3.CreateFile();

            js3 = new JsonRepository(question1);

            js3.CreateFile();
        }
    }
}
