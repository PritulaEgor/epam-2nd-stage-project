﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCreator
{
    class Topic
    {
        public Topic(string testName, string name)
        {
            this.testName = testName;
            this.name = name;
        }

        private string name;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public readonly string testName;

        private int numberOfQuestions;
        public int NumberOfQuestions
        {
            get
            {
                return numberOfQuestions;
            }
            set
            {
               if(value>0)
                {
                    numberOfQuestions = value;
                }
            }
        }

        private int countOfSubtopics=0;

        private int countOfQuestions = 0;

        //private List<SubTopic> subTopicList;

        //private List<Question> questionList;

    }
}
