﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCreator
{
    class SubTopic 
    {

        public SubTopic(string topicName, string testName, string name)
        {
            this.topicName = topicName;

            this.name = name;

            this.testName = testName;
        }

        public  string topicName;

        public string TopictName
        {
            get
            {
                return topicName;
            }
            set
            {
                topicName = value;
            }
        }

        private string testName;
        public string TestName
        {
            get
            {
                return testName;
            }
            set
            {
                testName = value;
            }
        }

        private string name;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        private int numberOfQuestions;
        public int NumberOfQuestions
        {
            get
            {
                return numberOfQuestions;
            }
            set
            {
                if (value > 0)
                {
                    numberOfQuestions = value;
                }
            }
        }

        private int countOfSubtopics = 0;

        private int countOfQuestions = 0;

        //private List<SubTopic> subTopicList;

        //private List<Question> questionList;
    }
}
